@echo off
cls
echo 0 - Deleting old CDI
del sequence.cdi
echo 1 - Converting ELF to BIN
tools\elf2bin -s 8C010000 1st_read.elf
move 1st_read.bin tools\1st_read.bin
cd tools
echo 2 - Removing GDROM protections
hexpatcher 5EB00000 4C2E0000 1st_read.bin -nobackup
hexpatcher 6EB00000 A6000000 1st_read.bin -nobackup
hexpatcher CDE4436A 09000900 1st_read.bin -nobackup
hexpatcher 10320D8B 08000D8B 1st_read.bin -nobackup
echo 3 - Creating a hacked IP.BIN with NeoIP
neoip ip_original.bin 1st_read.bin ip.bin
move ip.bin ..\data\ip.bin
move 1st_read.bin ..\data\1st_read.bin
echo 4 - Building a CDI
dcdimake -i ..\data -o ..\test.cdi
cd ..
del data\ip.bin
del data\1st_read.bin
D:\Emu\Demul\demul.exe -run=dc -image=test.cdi
#include <shinobi.h>
#include "njdef.h"
#include "tea_teapot1.nja"

extern NJS_OBJECT object_tea_Teapot1_Teapot1[];
extern NJS_MATRIX matrix[8];
extern NJS_VERTEX_BUF vbuf[4096];

NJS_VIEW view;

bool rotate = true; // True if the Simple light needs to be rotated

NJS_LIGHT lightE; // First light
NJS_LIGHT lightD; // Second light

Angle r_lightEx, r_lightEy, r_lightEz = 0; // Simple light rotation XYZ

NJS_VECTOR v_lightE = { -0.6f, 0.866f, 0.0f }; // First light direction
NJS_VECTOR v_lightD = { -0.6f, 0.866f, 0.0f }; // Second light direction
NJS_VECTOR v_lightEasy = { -0.6f, 0.866f, 0.0f }; // Easy light direction (2D)
NJS_VECTOR v_lightEasy_n = { 0.0f, 0.0f, 0.0f }; // Easy light direction (3D)

NJS_ARGB c_lightE = { 1.0f, 0.0f, 1.0f, 0.0f }; // First light color
NJS_ARGB c_lightD = { 1.0f, 1.0f, 1.0f, 0.0f }; // Second light color
NJS_ARGB c_lightEasy = { 1.0f, 0.0f, 1.0f, 1.0f }; // Easy light color

float lightE_a = 0.0f; // First light ambient
float lightE_d = 1.0f; // First light diffuse
float lightE_s = 1.0f; // First light specular

float lightD_a = 0.0f; // Second light ambient
float lightD_d = 0.25f; // Second light diffuse
float lightD_s = 1.0f; // Second light specular

float lightEasy_a = 0.0f; // Easy light ambient
float lightEasy_d = 0.5f;  // Easy light diffuse

Sint32 xx, yy, zz = 0; // Teapot rotation
const char* modename[] = { "TWO LIGHTS", "SIMPLE", "EASY" };
Uint8 mode = 0; // 0 - two lights, 1 - simple, 2 - easy
Sint8 selection = 0; // Current selection
bool show_true_dir = true; // Show njUnitVector'ed direction
bool hide_text = false; // Hide hints

char selection_y[] = { // Y coordinates for cursor
    7, 8, 9,    // XYZ for light 1
    11, 12, 13, // RGB for light 1 or P_XYZ for Simple
    15, 16, 17, // ADS for light 1
    22, 23, 24, // XYZ for light 2
    26, 27, 28, // RGB for light 2
    30, 31, 32  // ADS for light 2
};

void UpdateLighting() // Delete and recreate lights
{
    // Clean up all lights
    njDeleteAllLight();
    // Create the first light for two lights or simple light mode
    if (mode < 2)
    {
        // ENV light
        njCreateLight(&lightE, NJD_PHONG_DIR);
        njSetLightColor(&lightE, c_lightE.r, c_lightE.g, c_lightE.b);
        njSetLightIntensity(&lightE, lightE_s, lightE_d, lightE_a);
        njSetLightDirection(&lightE, v_lightE.x, v_lightE.y, v_lightE.z);
    }
    // Create the second light for two lights mode
    if (!mode)
    {
        // DIR light
        njCreateLight(&lightD, NJD_PHONG_DIR);
        njSetLightColor(&lightD, c_lightD.r, c_lightD.g, c_lightD.b);
        njSetLightIntensity(&lightD, lightD_s, lightD_d, lightD_a);
        njSetLightDirection(&lightD, v_lightD.x, v_lightD.y, v_lightD.z);
    }
}

float ClampFloat(float value)
{
    if (value < 0)
        return 0;
    else
        return value;
}

int ClampInt(int value, int max)
{
    if (value < 0)
        return 0;
    if (value > max)
        return max;
    return value;
}

void Input(const PDS_PERIPHERAL* pad)
{
    float valueadd = 0.0f;
    // Show or hide text
    if (pad->press & PDD_DGT_TX || pad->press & PDD_DGT_TY)
        hide_text = !hide_text;
    // Show njVector-ed direction
    if (pad->press & PDD_DGT_TL || pad->press & PDD_DGT_TR)
        show_true_dir = !show_true_dir;
    // Switch modes
    if (pad->press & PDD_DGT_ST)
    {
        mode++;
        if (mode > 2)
            mode = 0;
        UpdateLighting();
    }
    // Move teapot
    xx -= pad->y1 * 4;
    yy += pad->x1 * 4;
    // Move cursor
    if (pad->press & PDD_DGT_KD)
        selection++;
    if (pad->press & PDD_DGT_KU)
        selection--;
    switch (mode)
    {
        case 0:
            selection = ClampInt(selection, 17);
            break;
        case 1:
            selection = ClampInt(selection, 8);
            break;
        case 2:
            selection = ClampInt(selection, 7);
            break;
    }
    // Increase or decrease values
    if (pad->press & PDD_DGT_KR || pad->press & PDD_DGT_KL || pad->on & PDD_DGT_TA || pad->on & PDD_DGT_TB)
    {
        if (pad->press & PDD_DGT_KR)
            valueadd = 0.1f;
        else if (pad->press & PDD_DGT_KL)
            valueadd = -0.1f;
        else if (pad->on & PDD_DGT_TA)
            valueadd = 0.01f;
        else if (pad->on & PDD_DGT_TB)
            valueadd = -0.01f;
        switch (selection)
        {
            case 0:
                if (mode != 2)
                    v_lightE.x += valueadd;
                else
                    v_lightEasy.x += valueadd;
                break;
            case 1:
                if (mode != 2)
                    v_lightE.y += valueadd;
                else
                    v_lightEasy.y += valueadd;
                break;
            case 2:
                if (mode != 2)
                    v_lightE.z += valueadd;
                else
                    v_lightEasy.z += valueadd;
                break;
            case 3:
                if (mode == 0)
                    c_lightE.r += valueadd;
                else if (mode == 1)
                {
                    r_lightEx += valueadd * 10000;
                    rotate = true;
                    UpdateLighting();
                }
                else if (mode == 2)
                {
                    c_lightEasy.r += valueadd;
                }
                break;
            case 4:
                if (mode == 0)
                    c_lightE.g += valueadd;
                else if (mode == 1)
                {
                    r_lightEy += valueadd * 10000;
                    rotate = true;
                    UpdateLighting();
                }
                else if (mode == 2)
                {
                    c_lightEasy.g += valueadd;
                }
                break;
            case 5:
                if (mode == 0)
                    c_lightE.b += valueadd;
                else if (mode == 1)
                {
                    r_lightEz += valueadd * 10000;
                    rotate = true;
                    UpdateLighting();
                }
                else if (mode == 2)
                {
                    c_lightEasy.b += valueadd;
                }
                break;
            case 6:
                if (mode != 2)
                    lightE_a += valueadd;
                else
                    lightEasy_a += valueadd;
                break;
            case 7:
                if (mode != 2)
                    lightE_d += valueadd;
                else
                    lightEasy_d += valueadd;
                break;
            case 8:
                lightE_s += valueadd;
                break;
            case 9:
                v_lightD.x += valueadd;
                break;
            case 10:
                v_lightD.y += valueadd;
                break;
            case 11:
                v_lightD.z += valueadd;
                break;
            case 12:
                c_lightD.r += valueadd;
                break;
            case 13:
                c_lightD.g += valueadd;
                break;
            case 14:
                c_lightD.b += valueadd;
                break;
            case 15:
                lightD_a += valueadd;
                break;
            case 16:
                lightD_d += valueadd;
                break;
            case 17:
                lightD_s += valueadd;
                break;
        }
        c_lightE.r = ClampFloat(c_lightE.r);
        c_lightE.g = ClampFloat(c_lightE.g);
        c_lightE.b = ClampFloat(c_lightE.b);
        lightE_a = ClampFloat(lightE_a);
        lightE_d = ClampFloat(lightE_d);
        lightE_s = ClampFloat(lightE_s);
        c_lightD.r = ClampFloat(c_lightD.r);
        c_lightD.g = ClampFloat(c_lightD.g);
        c_lightD.b = ClampFloat(c_lightD.b);
        lightD_a = ClampFloat(lightD_a);
        lightD_d = ClampFloat(lightD_d);
        lightD_s = ClampFloat(lightD_s);
        c_lightEasy.r = ClampFloat(c_lightEasy.r);
        c_lightEasy.g = ClampFloat(c_lightEasy.g);
        c_lightEasy.b = ClampFloat(c_lightEasy.b);
        lightEasy_a = ClampFloat(lightEasy_a);
        lightEasy_d = ClampFloat(lightEasy_d);
        r_lightEx = ClampInt(r_lightEx, 65535);
        r_lightEy = ClampInt(r_lightEy, 65535);
        r_lightEz = ClampInt(r_lightEz, 65535);
        valueadd = 0;
    }
}

void Vblank() // Display text loop
{
    if (hide_text)
        return;
    njPrintColor(0xCC30F030);
    njPrint(NJM_LOCATION(0, selection_y[selection]), ">");
    njPrintColor(0xCCC0C0C0);
    njPrint(NJM_LOCATION(14, 1), "-- NINJA LIGHT TEST --");
    njPrint(NJM_LOCATION(1, 3),  "MODE: %s", modename[mode]);
    njPrint(NJM_LOCATION(1, 34), "Controls: ANALOG to rotate model,");
    njPrint(NJM_LOCATION(1, 35), "TRIGGERS to show LIGHT struct fields,");
    njPrint(NJM_LOCATION(1, 36), "START to change mode, UP / DOWN to move cursor,");
    njPrint(NJM_LOCATION(1, 37), "A / B / LEFT / RIGHT to change values");
    njPrint(NJM_LOCATION(1, 38), "X / Y to hide all text");
    switch (mode)
    {
        case 0: // Two lights
            // ENV light
            njPrintC(NJM_LOCATION(1, 5), "LIGHT 1");
            njPrint(NJM_LOCATION(1, 7),  "X  : %f", show_true_dir ? v_lightE.x : lightE.vctr.x);
            njPrint(NJM_LOCATION(1, 8),  "Y  : %f", show_true_dir ? v_lightE.y : lightE.vctr.y);
            njPrint(NJM_LOCATION(1, 9),  "Z  : %f", show_true_dir ? v_lightE.z : lightE.vctr.z);
            njPrint(NJM_LOCATION(1, 11), "R  : %f", show_true_dir ? c_lightE.r : lightE.attr.argb.r);
            njPrint(NJM_LOCATION(1, 12), "G  : %f", show_true_dir ? c_lightE.g : lightE.attr.argb.g);
            njPrint(NJM_LOCATION(1, 13), "B  : %f", show_true_dir ? c_lightE.b : lightE.attr.argb.b);
            njPrint(NJM_LOCATION(1, 15), "AMB: %f", show_true_dir ? lightE_a : lightE.attr.iamb);
            njPrint(NJM_LOCATION(1, 16), "DIF: %f", show_true_dir ? lightE_d : lightE.attr.idif);
            njPrint(NJM_LOCATION(1, 17), "SPC: %f", show_true_dir ? lightE_s : lightE.attr.ispc);
            // DIR light
            njPrintC(NJM_LOCATION(1, 20), "LIGHT 2");
            njPrint(NJM_LOCATION(1, 22),  "X  : %f", show_true_dir ? v_lightD.x : lightD.vctr.x);
            njPrint(NJM_LOCATION(1, 23),  "Y  : %f", show_true_dir ? v_lightD.y : lightD.vctr.y);
            njPrint(NJM_LOCATION(1, 24),  "Z  : %f", show_true_dir ? v_lightD.z : lightD.vctr.z);
            njPrint(NJM_LOCATION(1, 26),  "R  : %f", show_true_dir ? c_lightD.r : lightD.attr.argb.r);
            njPrint(NJM_LOCATION(1, 27),  "G  : %f", show_true_dir ? c_lightD.g : lightD.attr.argb.g);
            njPrint(NJM_LOCATION(1, 28),  "B  : %f", show_true_dir ? c_lightD.b : lightD.attr.argb.b);
            njPrint(NJM_LOCATION(1, 30),  "AMB: %f", show_true_dir ? lightD_a : lightD.attr.iamb);
            njPrint(NJM_LOCATION(1, 31),  "DIF: %f", show_true_dir ? lightD_d : lightD.attr.idif);
            njPrint(NJM_LOCATION(1, 32),  "SPC: %f", show_true_dir ? lightD_s : lightD.attr.ispc);
            break;
        case 1: // Simple mode
            njPrintC(NJM_LOCATION(1, 5), "SIMPLE LIGHT");
            njPrint(NJM_LOCATION(1, 7),  "X  : %f", show_true_dir ? v_lightE.x : lightE.vctr.x);
            njPrint(NJM_LOCATION(1, 8),  "Y  : %f", show_true_dir ? v_lightE.y : lightE.vctr.y);
            njPrint(NJM_LOCATION(1, 9),  "Z  : %f", show_true_dir ? v_lightE.z : lightE.vctr.z);
            njPrint(NJM_LOCATION(1, 11), "RX : %d", r_lightEx);
            njPrint(NJM_LOCATION(1, 12), "RY : %d", r_lightEy);
            njPrint(NJM_LOCATION(1, 13), "RZ : %d", r_lightEz);
            njPrint(NJM_LOCATION(1, 15), "AMB: %f", show_true_dir ? lightE_a : lightE.attr.iamb);
            njPrint(NJM_LOCATION(1, 16), "DIF: %f", show_true_dir ? lightE_d : lightE.attr.idif);
            njPrint(NJM_LOCATION(1, 17), "SPC: %f", show_true_dir ? lightE_s : lightE.attr.ispc);
            break;
        case 2: // Easy mode
            njPrintC(NJM_LOCATION(1, 5), "EASY LIGHT");
            njPrint(NJM_LOCATION(1, 7),  "X  : %f", v_lightEasy.x);
            njPrint(NJM_LOCATION(1, 8),  "Y  : %f", v_lightEasy.y);
            njPrint(NJM_LOCATION(1, 9),  "Z  : %f", v_lightEasy.z);
            njPrint(NJM_LOCATION(1, 11), "R  : %f", c_lightEasy.r);
            njPrint(NJM_LOCATION(1, 12), "G  : %f", c_lightEasy.g);
            njPrint(NJM_LOCATION(1, 13), "B  : %f", c_lightEasy.b);
            njPrint(NJM_LOCATION(1, 15), "AMB: %f", lightEasy_a);
            njPrint(NJM_LOCATION(1, 16), "DIF: %f", lightEasy_d);
            break;
    }
}

void njUserInit(void)
{
    njSetBorderColor(0);
    sbInitSystem(NJD_RESOLUTION_VGA, NJD_FRAMEBUFFER_MODE_RGB555, 1);
    njInitVertexBuffer(800000, 0, 200000, 0, 0);
    njInitPrint(NULL, 0, 0);
    njInitMatrix(matrix, 8, 0);
    njInit3D(vbuf, 4096);
    njInitView(&view);
    njSetView(&view);
    nwInitSystem(10, NJD_PORT_A0);
    njControl3D(NJD_CONTROL_3D_CONSTANT_ATTR);
    njSetConstantAttr(0xffffffff, NJD_FILTER_BILINEAR);
    njSetBackColor(0x00000000, 0x00000000, 0x000000FF);
    njSetVSyncFunction(Vblank);
    njPrintSize(12);
    UpdateLighting();
}

void DrawModel(int mode)
{
    njClearMatrix();
    njTranslate(NULL, 0.0f, 0.0f, -12.f);
    njRotateXYZ(NULL, xx, yy, zz);
    switch (mode)
    {
        case 0:
            njDrawObject(object_tea_Teapot1_Teapot1);
            break;
        case 1:
            njSimpleDrawObject(object_tea_Teapot1_Teapot1);
            break;
        case 2:
            njEasyDrawObject(object_tea_Teapot1_Teapot1);
            break;
    }
}

Sint32 njUserMain(void) // Render loop
{
    const PDS_PERIPHERAL* pad;
    pad = pdGetPeripheral(PDD_PORT_A0);
    njClearMatrix();
    Input(pad);
    switch (mode)
    {
        case 0:
            // ENV light
            njSetLightColor(&lightE, c_lightE.r, c_lightE.g, c_lightE.b);
            njSetLightIntensity(&lightE, lightE_s, lightE_d, lightE_a);
            njSetLightDirection(&lightE, v_lightE.x, v_lightE.y, v_lightE.z);
            // DIR light
            njSetLightColor(&lightD, c_lightD.r, c_lightD.g, c_lightD.b);
            njSetLightIntensity(&lightD, lightD_s, lightD_d, lightD_a);
            njSetLightDirection(&lightD, v_lightD.x, v_lightD.y, v_lightD.z);
            break;
        case 1:
            // Simple light
            njSetLightColor(&lightE, c_lightE.r, c_lightE.g, c_lightE.b);
            njSetLightIntensity(&lightE, lightE_s, lightE_d, lightE_a);
            njSetLightDirection(&lightE, v_lightE.x, v_lightE.y, v_lightE.z);
            if (rotate)
            {
                njRotateLightXYZ(&lightE, r_lightEx, r_lightEy, r_lightEz);
                rotate = false;
            }
            break;
        case 2: // Easy light
            njCalcVector(NULL, &v_lightEasy, &v_lightEasy_n);
            njSetEasyLight(v_lightEasy_n.x, v_lightEasy_n.y, v_lightEasy_n.z);
            njSetEasyLightColor(c_lightEasy.r, c_lightEasy.g, c_lightEasy.b);
            njSetEasyLightIntensity(lightEasy_d, lightEasy_a);
            break;
    }
    DrawModel(mode);
}

void njUserExit(void)
{
    nwExitSystem();
    njExitPrint();
    njExitTexture();
    sbExitSystem();
}